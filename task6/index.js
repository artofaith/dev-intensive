var express    = require('express');
var cors       = require('cors');
var fetch      = require('node-fetch');
var app = express();

app.use(cors());

var pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

var data;
fetch(pcUrl)
  .then(function(res) {
    return res.json();
  })
  .then(function(res) {
    data = res;
  })
  .catch(function(err) {
    console.log('Чтото пошло не так:', err);
  });

app.get("/", function(req, res) {
  res.send(data);
});

app.get("/board", function(req, res) {
  res.send(data.board);
});

app.get("/board/vendor", function(req, res) {
  res.send("\""+data.board.vendor+"\"");
});

app.get("/board/model", function(req, res) {
  res.send("\""+data.board.model+"\"");
});

app.get("/board/cpu", function(req, res) {
  res.send(data.board.cpu);
});

app.get("/board/image", function(req, res) {
  res.send("\""+data.board.image+"\"");
});

app.get("/board/video", function(req, res) {
  res.send("\""+data.board.video+"\"");
});

app.get("/ram", function(req, res) {
  res.send(data.ram);
});

app.get("/ram/vendor", function(req, res) {
  res.send("\""+data.ram.vendor+"\"");
});

app.get("/ram/volume", function(req, res) {
  res.json(data.ram.volume);
});

app.get("/ram/pins", function(req, res) {
  res.json(data.ram.pins);
});

app.get("/os", function(req, res) {
  res.send("\""+data.os+"\"");
});

app.get("/hdd", function(req, res) {
  res.send(data.hdd);
});

app.get("/volumes", function(req, res) {
  var volumes = data.hdd;
  var t = {
    "C:": volumes.map(function(v) {
      if(v.volume == "C:") {
        return v.size;
      } else  {
        return 0;
      }
    }).reduce(function(a,b){
      // console.log(arguments)
      return a+b;
    },0) + "B",
    "D:": volumes.map(function(v) {
      if(v.volume === "D:") {
        return v.size;
      } else  {
        return 0;
      }
    }).reduce(function(a,b){
      return a+b;
    },0) + "B"
  };

  res.send(t);
});

app.listen(80);
