var express    = require('express');
var cors       = require('cors');

var app = express();

app.use(cors());

app.get("/", function(req, res) {
  var fullname = req.query.fullname.replace(/  +/g, ' ').trim();
  var fullSplit = "";
  try {
    fullSplit = fullname.split(" ");
  } catch(e) {
    res.send(e)
  }
  if (fullSplit.length > 3 || fullSplit.length === 0 || fullname.length === 0) {
    res.send("Invalid fullname");
  } else {
    if ( 
      (fullSplit[fullSplit.length-1] ? fullSplit[fullSplit.length-1].match(/([0-9]|_|\/)/) ? true : false : false) ||
      (fullSplit[fullSplit.length-2] ? fullSplit[fullSplit.length-2].match(/([0-9]|_|\/)/) ? true : false : false) ||
      (fullSplit[fullSplit.length-3] ? fullSplit[fullSplit.length-3].match(/([0-9]|_|\/)/) ? true : false : false)
    ) { 
      res.send("Invalid fullname");
    } else {
      var lastName = fullSplit[fullSplit.length-1][0].toUpperCase() + fullSplit[fullSplit.length-1].slice(1).toLowerCase();
      var firstName = fullSplit[fullSplit.length-3] ? (" " + fullSplit[fullSplit.length-3][0].toUpperCase() + ".") : "";
      var secondName = fullSplit[fullSplit.length-2] ? (" " + fullSplit[fullSplit.length-2][0].toUpperCase() + ".") : "";

      res.send(lastName + firstName + secondName);
    }
  }
});

app.listen(80);
