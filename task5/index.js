var express    = require('express');
var cors       = require('cors');

var app = express();

app.use(cors());

app.get("/", function(req, res) {
  var link = req.query.username;

  var username =link.replace(/(.*?):?\/\//g,'').split("/");
  username = username.length === 1 ? username[0] : username[1] 
  username = username[0] === "@" ? username : "@" + username

  res.send(username);
});

app.listen(80);
