// var Skb = require('skb');
// var token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ODI4NmVlNDU1NWE5OTAwMTE2MzUzZmUiLCJ1c2VybmFtZSI6ImFydG9mYWl0aEBnbWFpbC5jb20iLCJyb2xlIjoidXNlciIsImlhdCI6MTQ3OTA0NDgzOH0.IPS6ZIc0V0enyE2Fwo170gld_76R-qCmpXaJdJXkhRU';
// var skb = new Skb(token);
// skb.taskHelloWorld('Test task');

var express    = require('express');
var cors       = require('cors');

var app = express();

app.use(cors());

app.get("/", function(req, res) {
  console.log(req.query)
  try {  
    var a = req.query.a ? parseInt(req.query.a) : 0 
    var b = req.query.b ? parseInt(req.query.b) : 0 
  } catch (e) {
    return res.send(e)
  }
  var sum = a + b;
  res.status = 200;
  res.send(sum + "");
});

app.listen(80);
